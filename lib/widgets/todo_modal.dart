import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';

class TodoModal extends StatefulWidget {
  final Function handleSubmit;
  final String title;
  final String task;
  DateTime date;
  Color color;

  TodoModal({
    this.title,
    this.handleSubmit,
    this.task,
    this.date,
    this.color,
  });

  @override
  State<TodoModal> createState() => _TodoModalState();
}

class _TodoModalState extends State<TodoModal> {
  TextEditingController _textController;

  @override
  void initState() {
    super.initState();
    _textController = new TextEditingController(text: widget.task);
  }

  void _showDatePicker() {
    showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2022),
      lastDate: DateTime.now(),
    ).then((value) {
      if (value == null) {
        return;
      }

      setState(() {
        widget.date = value;
      });
    });
  }

  void _showColorsModal() {
    showDialog(
      context: context,
      builder: (_) {
        return AlertDialog(
          content: Column(
            mainAxisSize: MainAxisSize
                .min, // Trick with height, so Picker fill min height of Alert
            children: [
              ColorPicker(
                pickerColor: Theme.of(context).primaryColor, //default color
                onColorChanged: (Color color) {
                  setState(() {
                    widget.color = color;
                  });
                },
              )
            ],
          ),
          actions: [
            RaisedButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text('Got it'),
              color: Theme.of(context).primaryColor,
              textColor: Theme.of(context).textTheme.button.color,
            )
          ],
        );
      },
    );
  }

  void _submitData() {
    if (_textController.text.isEmpty) {
      return;
    }

    final color =
        widget.color == null ? Theme.of(context).primaryColor : widget.color;
    final date = widget.date == null ? DateTime.now() : widget.date;

    widget.handleSubmit(_textController.text, date, color);

    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus
          ?.unfocus(), // Close keyboard when tap outside
      child: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.only(
              top: 20,
              left: 20,
              right: 20,
              bottom: MediaQuery.of(context).viewInsets.bottom + 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(bottom: 20),
                child: Text(
                  widget.title,
                  style: Theme.of(context).textTheme.headline4,
                  textAlign: TextAlign.center,
                ),
              ),
              TextField(
                controller: _textController,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Text',
                ),
                onSubmitted: (_) => _submitData(),
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                children: [
                  Expanded(
                    child: Text(
                      widget.date != null
                          ? DateFormat.yMMMEd().format(widget.date)
                          : 'No Date',
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                  ),
                  FlatButton(
                    onPressed: _showDatePicker,
                    child: Text('Choose date'),
                    textColor: Theme.of(context).primaryColor,
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    width: 50,
                    height: 20,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(20)),
                      color: widget.color != null
                          ? widget.color
                          : Theme.of(context).primaryColor,
                    ),
                  ),
                  FlatButton(
                    onPressed: _showColorsModal,
                    child: Text('Choose color'),
                    textColor: Theme.of(context).primaryColor,
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Align(
                alignment: Alignment.centerRight,
                child: RaisedButton(
                  onPressed: _submitData,
                  child: Text('Submit'),
                  color: Theme.of(context).primaryColor,
                  textColor: Theme.of(context).textTheme.button.color,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
