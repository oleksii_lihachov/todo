import 'package:flutter/material.dart';

class TabPanel extends StatelessWidget {
  final _title;
  final Function _handleTap;

  TabPanel(this._title, this._handleTap);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        ListTile(
          title: Text(
            _title,
            style: Theme.of(context).textTheme.headline5,
          ),
          trailing: IconButton(
            icon: Icon(Icons.keyboard_arrow_down_outlined),
            color: Theme.of(context).accentColor,
            onPressed: _handleTap,
          ),
        ),
        Divider(
          height: 2,
          color: Colors.black54,
        )
      ],
    );
  }
}
