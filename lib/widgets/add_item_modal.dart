import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:intl/intl.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';

class AddItemModal extends StatefulWidget {
  final Function _handleSubmit;

  AddItemModal(this._handleSubmit);

  @override
  State<AddItemModal> createState() => _AddItemModalState();
}

class _AddItemModalState extends State<AddItemModal> {
  final _taskController = TextEditingController();
  DateTime _selectedDate;
  Color _selectedColor;

  void _showDatePicker() {
    showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2022),
      lastDate: DateTime.now(),
    ).then((value) {
      if (value == null) {
        return;
      }

      setState(() {
        _selectedDate = value;
      });
    });
  }

  void _showColorsModal() {
    showDialog(
      context: context,
      builder: (_) {
        return AlertDialog(
          content: Column(
            // mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.min, // Trick with height, so Picker fill min height of Alert
            children: [
              ColorPicker(
                pickerColor: Theme.of(context).primaryColor, //default color
                onColorChanged: (Color color) {
                  setState(() {
                    _selectedColor = color;
                  });
                },
              )
            ],
          ),
          actions: [
            RaisedButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text('Got it'),
              color: Theme.of(context).primaryColor,
              textColor: Theme.of(context).textTheme.button.color,
            )
          ],
        );
      },
    );
  }

  void _submitData() {
    if (_taskController.text.isEmpty) {
      return;
    }

    final color = _selectedColor == null
        ? Theme.of(context).primaryColor
        : _selectedColor;
    final date = _selectedDate == null ? DateTime.now() : _selectedDate;

    widget._handleSubmit(_taskController.text, date, color);

    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.only(
              top: 20,
              left: 20,
              right: 20,
              bottom: MediaQuery.of(context).viewInsets.bottom + 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(bottom: 20),
                child: Text(
                  'Add new Task',
                  style: Theme.of(context).textTheme.headline4,
                  textAlign: TextAlign.center,
                ),
              ),
              TextField(
                controller: _taskController,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Text',
                ),
                onSubmitted: (_) => _submitData(),
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                children: [
                  Expanded(
                    child: Text(
                      _selectedDate != null
                          ? DateFormat.yMMMEd().format(_selectedDate)
                          : 'No Date',
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                  ),
                  FlatButton(
                    onPressed: _showDatePicker,
                    child: Text('Choose a date'),
                    textColor: Theme.of(context).primaryColor,
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    width: 50,
                    height: 20,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(20)),
                      color: _selectedColor != null
                          ? _selectedColor
                          : Theme.of(context).primaryColor,
                    ),
                  ),
                  FlatButton(
                    onPressed: _showColorsModal,
                    child: Text('Select color'),
                    textColor: Theme.of(context).primaryColor,
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Align(
                alignment: Alignment.centerRight,
                child: RaisedButton(
                  onPressed: _submitData,
                  child: Text('Submit'),
                  color: Theme.of(context).primaryColor,
                  textColor: Theme.of(context).textTheme.button.color,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
