import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../models/todo_item.dart';

class TodoList extends StatelessWidget {
  final List<TodoItem> _list;
  final Function _toggleCompleted;
  final Function _deleteItem;
  final Function _openEditModal;

  TodoList(
    this._list,
    this._toggleCompleted,
    this._deleteItem,
    this._openEditModal,
  );

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: _list.length,
      itemBuilder: (ctx, index) {
        return Card(
          elevation: 4,
          color: _list[index].isCompleted ? Colors.grey[300] : null,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20),
          ),
          margin: EdgeInsets.symmetric(
            vertical: 10,
            horizontal: 15,
          ),
          clipBehavior: Clip.antiAlias, // Cut container borders across Card
          child: IntrinsicHeight(
            // Need for add some constains for Row and then Container with
            child: Row(
              // color can fill height
              children: [
                Container(
                  width: 20,
                  decoration: BoxDecoration(color: _list[index].color),
                ),
                Checkbox(
                  value: _list[index].isCompleted,
                  onChanged: (val) {
                    _toggleCompleted(val, _list[index].id);
                  },
                ),
                Expanded(
                  child: Container(
                    padding: EdgeInsets.only(top: 10, bottom: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          _list[index].task,
                          style: Theme.of(context).textTheme.headline6.copyWith(
                              decoration: _list[index].isCompleted
                                  ? TextDecoration.lineThrough
                                  : null),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          DateFormat('d MMMM').format(_list[index].date),
                          style: TextStyle(
                            color: Colors.black54,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                IconButton(
                  onPressed: () => _openEditModal(
                    context,
                    _list[index].id,
                    _list[index].task,
                    _list[index].date,
                    _list[index].color,
                  ),
                  icon: Icon(Icons.edit),
                  color: Colors.grey,
                  splashRadius: 25,
                ),
                IconButton(
                  onPressed: () => _deleteItem(_list[index].id),
                  icon: Icon(Icons.delete_outline),
                  color: Theme.of(context).errorColor,
                  splashRadius: 25,
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
