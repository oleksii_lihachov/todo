import 'package:flutter/material.dart';
import 'package:uuid/uuid.dart';

import './widgets/todo_modal.dart';
import './widgets/tab_panel.dart';
import './widgets/todo_list.dart';
import './models/todo_item.dart';

class Home extends StatefulWidget {
  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final Uuid uuid = Uuid();
  final List<TodoItem> _todoItems = [];

  var _showCompleted = false;
  var _showUncomplete = false;

  void _toggleStatus(bool value, String id) {
    final index = _todoItems.indexWhere((element) => element.id == id);

    setState(() {
      _todoItems[index].isCompleted = value;
    });
  }

  void _deleteItem(String id) {
    setState(() {
      _todoItems.removeWhere((element) => element.id == id);
    });
  }

  void _addItem(String task, DateTime date, Color color) {
    final newItem = TodoItem(
      id: uuid.v1(),
      task: task,
      isCompleted: false,
      date: date,
      color: color,
    );

    setState(() {
      _todoItems.add(newItem);
    });
  }

  void _editItem(String id, String task, DateTime date, Color color) {
    final index = _todoItems.indexWhere((element) => element.id == id);

    setState(() {
      _todoItems[index].task = task;
      _todoItems[index].date = date;
      _todoItems[index].color = color;
    });
  }

  void _toggleCompleted() {
    setState(() {
      _showCompleted = !_showCompleted;
      _showUncomplete = false;
    });
  }

  void _toggleUncompleted() {
    setState(() {
      _showUncomplete = !_showUncomplete;
      _showCompleted = false;
    });
  }

  void _openAddModal(BuildContext ctx) {
    showModalBottomSheet(
        context: ctx,
        builder: (_) {
          return TodoModal(
            handleSubmit: _addItem,
            title: 'Add New Item',
          );
        });
  }

  void _openEditModal(
    BuildContext ctx,
    String id,
    String task,
    DateTime date,
    Color color,
  ) {
    showModalBottomSheet(
        context: ctx,
        builder: (_) {
          return TodoModal(
            handleSubmit: (submittedTask, submittedDate, submitteColor) =>
                _editItem(id, submittedTask, submittedDate, submitteColor),
            title: 'Edit Item',
            task: task,
            date: date,
            color: color,
          );
        });
  }

  List<TodoItem> get _completedItems {
    return _todoItems.where((element) => element.isCompleted == true).toList();
  }

  List<TodoItem> get _unCompletedItems {
    return _todoItems.where((element) => element.isCompleted == false).toList();
  }

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);

    final appBar = AppBar(
      title: Text('ToDo List'),
    );

    return Scaffold(
      appBar: appBar,
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              height: (mediaQuery.size.height -
                      appBar.preferredSize.height -
                      mediaQuery.padding.top) *
                  0.1,
              child: TabPanel('Uncompleted:', _toggleUncompleted),
            ),
            if (_showUncomplete)
              Container(
                height: (mediaQuery.size.height -
                        appBar.preferredSize.height -
                        mediaQuery.padding.top) *
                    0.8,
                child: TodoList(_unCompletedItems, _toggleStatus, _deleteItem,
                    _openEditModal),
              ),
            Container(
              height: (mediaQuery.size.height -
                      appBar.preferredSize.height -
                      mediaQuery.padding.top) *
                  0.1,
              child: TabPanel('Completed:', _toggleCompleted),
            ),
            if (_showCompleted)
              Container(
                height: (mediaQuery.size.height -
                        appBar.preferredSize.height -
                        mediaQuery.padding.top) *
                    0.8,
                child: TodoList(_completedItems, _toggleStatus, _deleteItem,
                    _openEditModal),
              ),
          ],
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add_box_sharp),
        onPressed: () => _openAddModal(context),
      ),
    );
  }
}
