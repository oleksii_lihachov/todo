import 'package:flutter/material.dart';

class TodoItem {
  final String id;
  String task;
  bool isCompleted;
  DateTime date;
  Color color;

  TodoItem({
    @required this.id,
    @required this.task,
    @required this.isCompleted,
    @required this.date,
    @required this.color,
  });
}
