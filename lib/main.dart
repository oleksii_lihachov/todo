import 'package:flutter/material.dart';

import './home.dart';

void main() {
  runApp(App());
}

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'ToDo List',
      theme: ThemeData(
        accentColor: Colors.brown,
        errorColor: Colors.red[700],
        fontFamily: 'Fredoka',
        textTheme: TextTheme(
            headline4: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.w500,
              color: Colors.black87,
            ),
            headline5: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.w500,
            ),
            headline6: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.w400,
            ),
            button: TextStyle(
              color: Colors.white,
            )),
      ),
      home: Home(),
    );
  }
}
